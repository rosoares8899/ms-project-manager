package br.projectmanager;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class ProjectResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/projects/hello")
          .then()
          .statusCode(200)
          .body(is("hello"));
    }

    @Test
    public void whenGetProjects_thenResponseShouldBeSuccessfully() {
        given()
          .when().get("projects/")
          .then()
          .statusCode(200);
    }

    @Test
    public void whenCreateProject_thenResponseShouldBeCreated() {
        JsonObject requestParams = new JsonObject();
        requestParams.put("name", "Test name");
        requestParams.put("cod", "1234");
        requestParams.put("description", "My description");

        Response response = given()
          .header("Content-type", ContentType.JSON)
          .and()
          .body(requestParams.toString())
          .when().post("projects/store");

        Assertions.assertEquals(201, response.getStatusCode());
    }

}
