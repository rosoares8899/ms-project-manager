package br.projectmanager.project;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/projects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("hello")
    public String hello() {
        return "hello";
    }

    @GET
    public List<Project> list() {
        return Project.listAll();
    }

    @POST
    @Path("/store")
    @Transactional
    public Response create(Project project) {
        project.persist();
        return Response.created(URI.create("projects/" + project.id)).build();
    }

    @DELETE
    @Path("/delete/{id}")
    @Transactional
    public void delete(@PathParam(("id")) Long id) {
        Project project = Project.findById(id);
        if (project == null) {
            throw new NotFoundException();
        }
        project.delete();
    }

}
